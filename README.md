# Example Openweather
This project is in progress.

## Run the project
Import the `pom.xml` file in your IDE and launch the application.

## Access the API
This service run on `localhost:8080`. You can access the API with the endpoint `/api/weather?city=name`.

Example url: http://localhost:8080/api/weather?city=wiesbaden