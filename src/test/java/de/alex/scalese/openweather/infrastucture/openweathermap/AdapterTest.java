package de.alex.scalese.openweather.infrastucture.openweathermap;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import de.alex.scalese.openweather.domain.Weather;
import org.junit.jupiter.api.*;
import org.springframework.http.HttpStatus;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.assertj.core.api.Assertions.assertThat;

class AdapterTest {

    private static final String OPERNWEATHER_API_PATH = "/data/2.5";
    private static final String API_KEY = "testKey";
    private static final String CITY = "Wiesbaden";
    private static final String JSON_FIXTURE = "{\"coord\":{\"lon\":8.25,\"lat\":50.08},\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"base\":\"stations\",\"main\":{\"temp\":14.79,\"feels_like\":9.44,\"temp_min\":12.22,\"temp_max\":16.11,\"pressure\":1013,\"humidity\":54},\"visibility\":10000,\"wind\":{\"speed\":6.2,\"deg\":260,\"gust\":9.8},\"clouds\":{\"all\":90},\"dt\":1602075540,\"sys\":{\"type\":1,\"id\":1857,\"country\":\"DE\",\"sunrise\":1602048996,\"sunset\":1602089556},\"timezone\":7200,\"id\":2809346,\"name\":\"Wiesbaden\",\"cod\":200}";

    private static final WireMockServer server = new WireMockServer(options().dynamicPort());

    private ApiClient apiClient;

    private Adapter adapter;

    @BeforeAll
    private static void startServer() {
        server.start();
    }

    @BeforeEach
    private void setup() {
        apiClient = new ApiClient();
        apiClient.setApikey(API_KEY);
        apiClient.setUrl("http://localhost:" + server.port() + "" + OPERNWEATHER_API_PATH);
        adapter = new Adapter(apiClient);
    }

    @AfterEach
    void after() {
        server.resetAll();
    }

    @AfterAll
    static void clean() {
        server.shutdown();
    }

    @Test
    void getWeather() {
        server.stubFor(
                WireMock.get(OPERNWEATHER_API_PATH + "/weather?appid=" + API_KEY + "&q=" + CITY + "&units=metric")
                        .willReturn(WireMock.aResponse()
                                .withStatus(HttpStatus.OK.value())
                                .withBody(JSON_FIXTURE)
                        ));

        assertThat(this.adapter.GetWeather(CITY)).isEqualToComparingFieldByField(new Weather(CITY, "DE", "overcast clouds", 14.79));
    }

}