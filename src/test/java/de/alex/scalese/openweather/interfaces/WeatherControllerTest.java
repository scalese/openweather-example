package de.alex.scalese.openweather.interfaces;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.alex.scalese.openweather.application.WeatherService;
import de.alex.scalese.openweather.domain.Weather;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ExtendWith(MockitoExtension.class)
class WeatherControllerTest {

    private MockMvc mvc;

    @Mock
    private WeatherService weatherService;

    @InjectMocks
    private WeatherController weatherController;

    private JacksonTester<Weather> jsonWeather;

    private static final String CITY = "Wiesbaden";
    private static final String COUNTRY_CODE = "DE";
    private static final String DESCRIPTION = "description";
    private static final double TEMPERATURE = 14.7;


    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());

        mvc = MockMvcBuilders.standaloneSetup(weatherController).build();
    }

    @Test
    public void getWeather() throws Exception {
        BDDMockito.given(weatherService.getWeatherByCity(CITY))
                .willReturn(new Weather(CITY, COUNTRY_CODE, DESCRIPTION, TEMPERATURE));

        MockHttpServletResponse response = mvc.perform(
                get("/weather?city=" + CITY).accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo(
                jsonWeather.write(new Weather(CITY, COUNTRY_CODE, DESCRIPTION, TEMPERATURE))
                        .getJson()
        );
    }

    @Test
    public void requestWithoutCity() throws Exception {
        MockHttpServletResponse response = mvc.perform(
                get("/weather").accept(MediaType.APPLICATION_JSON)
        ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo(
                jsonWeather.write(new Weather()).getJson()
        );
    }
}