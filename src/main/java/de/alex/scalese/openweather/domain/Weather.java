package de.alex.scalese.openweather.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class Weather {

    private String locationName;
    private String locationCountryCode;
    private String description;
    private double temperatureInCelsius;

}
