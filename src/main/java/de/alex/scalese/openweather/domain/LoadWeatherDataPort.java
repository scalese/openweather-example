package de.alex.scalese.openweather.domain;

public interface LoadWeatherDataPort {
    public Weather GetWeather(String city);
}
