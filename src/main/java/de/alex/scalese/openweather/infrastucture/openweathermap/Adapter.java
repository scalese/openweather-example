package de.alex.scalese.openweather.infrastucture.openweathermap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.alex.scalese.openweather.domain.LoadWeatherDataPort;
import de.alex.scalese.openweather.domain.Weather;
import de.alex.scalese.openweather.infrastucture.openweathermap.dto.Openweathermap;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.net.http.HttpResponse;

@Service
@Qualifier("OpenweathermapAdapter")
public class Adapter implements LoadWeatherDataPort {

    private final ApiClient apiClient;

    public Adapter(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    @Override
    public Weather GetWeather(String city) {

        HttpResponse<String> response = apiClient.request(city);
        return response.statusCode() == 200 ? mapBody(response.body()) : new Weather();
    }

    private Weather mapBody(String body) {

        ObjectMapper mapper = new ObjectMapper();
        try {
            Openweathermap dto = mapper.readValue(body, Openweathermap.class);
            return mapDto(dto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return new Weather();
    }

    private Weather mapDto(Openweathermap dto) {
        return new Weather(
                dto.getName(),
                dto.getSys().getCountry(),
                dto.getWeather().get(0).getDescription(),
                dto.getMain().getTemp());
    }
}
