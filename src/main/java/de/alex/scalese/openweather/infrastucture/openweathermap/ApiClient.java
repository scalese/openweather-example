package de.alex.scalese.openweather.infrastucture.openweathermap;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@NoArgsConstructor
@Component
@ConfigurationProperties("openweathermap")
public class ApiClient {

    @Setter
    private String url;
    @Setter
    private String apikey;

    public HttpResponse<String> request(String city) {

        try {
            // Further information:
            // https://openjdk.java.net/groups/net/httpclient/recipes.html
            URI uri = new URI(url + "/weather?appid=" + apikey + "&q=" + city + "&units=metric");
            HttpRequest request = HttpRequest.newBuilder(uri)
                    .header("Accept", "application/json")
                    .GET()
                    .build();

            return HttpClient.newHttpClient()
                    .send(request, HttpResponse.BodyHandlers.ofString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
