package de.alex.scalese.openweather.infrastucture;

import de.alex.scalese.openweather.domain.LoadWeatherDataPort;
import de.alex.scalese.openweather.domain.Weather;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("FakeAdapter")
public class FakeAdapter implements LoadWeatherDataPort {

    @Override
    public Weather GetWeather(String city) {
        return new Weather(city, "fake-DE", "Faked description. It is sunny...", 22.3);
    }
}
