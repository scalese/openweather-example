package de.alex.scalese.openweather.application;

import de.alex.scalese.openweather.domain.LoadWeatherDataPort;
import de.alex.scalese.openweather.domain.Weather;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class WeatherService {

    private final LoadWeatherDataPort service;

    public WeatherService(@Qualifier("OpenweathermapAdapter") LoadWeatherDataPort service) {
        this.service = service;
    }

    public Weather getWeatherByCity(String city) {
        return service.GetWeather(city);
    }

}

