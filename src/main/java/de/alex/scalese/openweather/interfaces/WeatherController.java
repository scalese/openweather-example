package de.alex.scalese.openweather.interfaces;

import de.alex.scalese.openweather.application.WeatherService;
import de.alex.scalese.openweather.domain.Weather;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class WeatherController {

    private final WeatherService service;

    public WeatherController(WeatherService service) {
        this.service = service;
    }

    @GetMapping("/weather")
    public Weather get(@RequestParam(defaultValue = "") String city) {

        return Objects.equals(city, "")
                ? new Weather()
                : service.getWeatherByCity(city);
    }

}
